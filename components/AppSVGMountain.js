const AppSVGMountain = () => {
    return (
        <div>
            <svg viewBox="0 0 2560 280" fill="#ffffff" stroke="#ffffff" xmlns="http://www.w3.org/2000/svg">
                <path d="M2560 251.547V279.5H0V256.187H0.0400391V254.093C14.8267 254.453 30.12 254.44 45.8667 254.093C446.973 245.08 1144.69 13.52 1173.91 15.7467C1240.56 20.68 1304.05 15.4399 1364.91 -0.00012207H1364.92C1426.67 3.42654 1448.17 14.4799 1448.17 14.4799C1589.23 59.0799 1639.99 74.3599 1647.41 77.3466C1881.93 175.693 2309.12 263.853 2560 251.547Z" fill="#ffffff"/>
            </svg>
            <div className="bg-white py-px absolute bottom-0 left-0 right-0"></div>
        </div>
    )
}
 
export default AppSVGMountain;