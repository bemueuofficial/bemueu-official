import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import sanitizeHTML from 'sanitize-html'

const AppTooltip = ({buttonText, tooltipText, tooltipPosition, buttonCustomClass, buttonParentCustomClass, isHTML, themeName, isArrow}) => {
    return (
        <div>
            <Tippy content={<div dangerouslySetInnerHTML={{__html: sanitizeHTML(tooltipText)}} />} interactive={true} placement={tooltipPosition || "auto"} theme={themeName || "amities"} allowHTML={isHTML} arrow={isArrow}>
                <div className={buttonParentCustomClass}>
                    <button className={buttonCustomClass}>
                        { buttonText }
                    </button>
                </div>
            </Tippy>
        </div>
    )
}
 
export default AppTooltip;