const { FontAwesomeIcon } = require("@fortawesome/react-fontawesome")

const AppFooter = () => {
    return (
        <footer className="flex-1 bg-black">
            <div className="container flex-1 md:flex flex-row mx-auto pt-10">
                <div className="flex items-center text-white px-6 lg:px-4 xl:px-0 px-auto text-center lg:text-left flex-col lg:flex-row">
                    <FontAwesomeIcon icon="cocktail" size="4x" className="mr-0 lg:mr-3"></FontAwesomeIcon>
                    <div>
                        <h2 className="text-2xl">This is a footer title you dummies</h2>
                        <p>lorem ipsum dolor sit amet.</p>
                        <div>
                            <small>lorem ipsum dolor sit amet consectetur adisipicing elit.</small>
                        </div>
                    </div>
                </div>
                <div className="flex md:flex-1 flex-wrap items-center justify-center lg:justify-end mt-3 md:mt-0">
                    <div className="px-3 py-3 lg:py-0">
                        <span className="fa-layers text-5xl lg:text-7xl">
                            <FontAwesomeIcon icon="circle" color="white"></FontAwesomeIcon>
                            <FontAwesomeIcon icon="cocktail" color="black" transform="shrink-6"></FontAwesomeIcon>
                        </span>
                    </div>
                    <div className="px-3 py-3 lg:py-0">
                        <span className="fa-layers text-5xl lg:text-7xl">
                            <FontAwesomeIcon icon="circle" color="white"></FontAwesomeIcon>
                            <FontAwesomeIcon icon="cocktail" color="black" transform="shrink-6"></FontAwesomeIcon>
                        </span>
                    </div>
                    <div className="px-3 py-3 lg:py-0">
                        <span className="fa-layers text-5xl lg:text-7xl">
                            <FontAwesomeIcon icon="circle" color="white"></FontAwesomeIcon>
                            <FontAwesomeIcon icon="cocktail" color="black" transform="shrink-6"></FontAwesomeIcon>
                        </span>
                    </div>
                    <div className="px-3 py-3 lg:py-0">
                        <span className="fa-layers text-5xl lg:text-7xl">
                            <FontAwesomeIcon icon="circle" color="white"></FontAwesomeIcon>
                            <FontAwesomeIcon icon="cocktail" color="black" transform="shrink-6"></FontAwesomeIcon>
                        </span>
                    </div>
                </div>
            </div>
            <div className="container flex-1 mx-auto pb-10 text-center md:text-right mt-5 md:pr-10 lg:pr-3">
                <p className="text-white">Copyright Amities 2020.</p>
            </div>
        </footer>
    )
}
 
export default AppFooter;