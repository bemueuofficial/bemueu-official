import AppNavbar from '../components/AppNavbar'
import AppHeader from '../components/AppHeader'
import AppFooter from '../components/AppFooter'
import AppTooltip from '../components/AppTooltip'
import AppAnimatedBackground from '../components/AppAnimatedBackground'

const tooltipArrayExample = [
    { title: "first button", tooltip: "lorem lorem"},
    { title: "second button", tooltip: "ipsum ipsum"},
    { title: "third button", tooltip: "dolor dolor"},
    { title: "fourth button", tooltip: "sit sit"},
    { title: "fifth button", tooltip: "amet amet"},
]

const IndexPage = () => {
    return (
        <div>
            <AppNavbar />
            <AppHeader />
            <div className="py-5 bg-white flex-1 flex-col text-center">
                <span className="text-4xl">1</span>
                <h2>Single Collumn</h2>
            </div>
            <div className="py-5 bg-white flex-1 md:flex flex-row">
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">1</span>
                    <h2>First Collumn</h2>
                </div>
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">2</span>
                    <h2>Second Collumn</h2>
                </div>
            </div>
            <div className="py-5 bg-white flex-1 md:flex flex-row">
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">1</span>
                    <h2>First Collumn</h2>
                </div>
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">2</span>
                    <h2>Second Collumn</h2>
                </div>
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">3</span>
                    <h2>Third Collumn</h2>
                </div>
                <div className="flex-1 flex-col text-center">
                    <span className="text-4xl">4</span>
                    <h2>Fourth Collumn</h2>
                </div>
            </div>
            <AppAnimatedBackground />
            <div className="container mx-auto py-5">
                <div className="grid grid-col grid-cols-1 md:grid-cols-3">
                    {
                        tooltipArrayExample.map(({title, tooltip}) => (
                            <AppTooltip key={`${title}${tooltip}`} buttonText={title} tooltipText={tooltip} tooltipPosition="top" buttonCustomClass="w-full bg-white p-5" buttonParentCustomClass="m-2" isArrow={true} />
                        ))
                    }
                </div>
            </div>
            <AppFooter />
        </div>
    )
}
 
export default IndexPage;