import { getAllPostIds, getPostData } from '../../lib/posts'
import sanitizeHTML from 'sanitize-html'
import Head from 'next/head'
import Date from '../../components/AppDate'
import AppFooter from '../../components/AppFooter'
import AppNavbar from '../../components/AppNavbar'
import AppComment from '../../components/AppComment'

export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
        paths,
        fallback: false
    }
}

export async function getStaticProps({ params }) {
    const postData = await getPostData(params.id)
    return {
        props: {
            postData
        }
    }
}

const Post = ({postData}) => {
    return (
        <>
            <Head>{postData.title}</Head>
            <AppNavbar /> 
            <div className="container mx-auto py-5 w-10/12 lg:w-3/6">
                <div className="mt-6 prose prose-sm sm:prose lg:prose-lg xl:prose-xl mx-auto">
                    <h3>{postData.title}</h3> 
                    <span>By <strong>{postData.author}</strong> at </span>
                    <Date dateString={postData.date} />
                    <div className="mt-6" dangerouslySetInnerHTML={{__html: sanitizeHTML(postData.contentHtml)}} />
                </div>
                <AppComment />
            </div>
            <AppFooter />
        </>
    )
}
 
export default Post;