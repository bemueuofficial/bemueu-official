import { getSortedPostsData } from '../lib/posts'
import AppNavbar from '../components/AppNavbar'
import AppFooter from '../components/AppFooter'
import Link from 'next/link'
import Date from '../components/AppDate'
import { useState, useEffect } from 'react'
import AppInstagram from '../components/AppInstagram'
import axios from 'axios'

export async function getStaticProps() {

    let instagramFetch = []

    const allPostsData = getSortedPostsData()

    try {
        const res = await axios.get('https://www.instagram.com/bemueu_official/')
        const data = JSON.parse(res.data.match(/<script type="text\/javascript">window\._sharedData = (.*)<\/script>/)[1].slice(0, -1))
        instagramFetch = data.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges.splice(0, 9)
    } catch (error) {
        console.error('Instagram Posts Fecth Failed. ' + error.toString())
    }
    return {
        props: {
            allPostsData,
            instagramFetch
        }
    }
}

const blog = ({allPostsData, instagramFetch}) => {

    const [currentAuthor, setCurrentAuthor] = useState(null)
    const [authors, setAuthors] = useState([])
    const [firstLoad, setFirstLoad] = useState(true)
    const filterPosts = (refString) => {
        if (firstLoad) {
            setFirstLoad((x) => !x)
            setAuthors(allPostsData)
        } else {
            if (refString == "") {
                setAuthors(allPostsData)
            } else {
                const thisShit = allPostsData.filter((x) => x.author == refString)
                setAuthors(thisShit)
            }
        }
    }

    useEffect(() => {
        filterPosts(currentAuthor, firstLoad)
    }, [currentAuthor])

    return (
        <>
            <AppNavbar />
            <div className="container mx-auto py-5 px-3 xl:px-0 grid flex-1 flex-row lg:grid-cols-12">
                <div className="flex-1 order-2 lg:col-span-9 lg:order-1">
                    {
                        authors.map(post => (
                            <div className="mb-5" key={post.id}>
                                <Link href={`/posts/${post.id}`}>
                                    <a className="text-3xl">{post.title}</a>
                                </Link>
                                <div className="mt-2">Oleh <strong>{post.author}</strong></div>
                                <Date dateString={post.date} />
                            </div>
                        ))
                    }
                </div>
                <div className="flex-1 order-1 lg:col-span-3 lg:order-2 p-3 border mb-3 lg:mb-0">
                    <div className="mb-2">
                        <label htmlFor="author-filter">Filter by Author</label>
                    </div>
                    <select name="authorFilter" id="author-filter" className="w-full" onChange={(e) => setCurrentAuthor(e.target.value)}>
                        <option></option>
                        {
                            allPostsData.map(post => (
                                <option value={post.author} key={post.id}>{post.author}</option>
                            ))
                        }
                    </select>
                </div>
            </div>
            <div className="container mx-auto py-5 w-7/12">
                <div className="grid grid-col grid-cols-1 md:grid-cols-3">
                    {
                        instagramFetch.map(item => (
                            <AppInstagram sourceElement={item} key={item.node.id} />
                        ))
                    }
                </div>
            </div>
            <AppFooter />
        </>
    )
}
 
export default blog;