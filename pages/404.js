import AppNavbar from '../components/AppNavbar'
import AppFooter from '../components/AppFooter'

const App404 = () => {
    return (
        <>
            <AppNavbar />
            <h1>this is a 404 page, you know why you're here ain't you ?</h1>
            <AppFooter />
        </>
    )
}
 
export default App404;